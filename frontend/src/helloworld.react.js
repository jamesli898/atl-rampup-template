import React from "react";
import axios from "axios";
import DynamicTable from "@atlaskit/dynamic-table";
import styled from "styled-components";

import { MenuGroup, Section, HeadingItem, ButtonItem } from "@atlaskit/menu";

const Wrapper = styled.div`
  min-width: 600px;
`;

class HelloWorld extends React.Component {
  state = {
    content: ""
  };

  componentDidMount() {
    axios.get("http://localhost:8080/").then(res => {
      if (res.data) {
        const content = res.data;
        console.dir(res.data);
        console.log(res.data);
        this.setState({ content: JSON.stringify(content) });
        return content;
      } else {
        console.log("No data found :(");
      }
    });
  }

  render() {
    return null;
    // <Wrapper>
    //   <DynamicTable
    //     caption={caption}
    //     head={head}
    //     rows={rows}
    //     rowsPerPage={10}
    //     defaultPage={1}
    //     //loadingSpinnerSize="large"
    //     isLoading={false}
    //     isFixedSize
    //     defaultSortKey="term"
    //     defaultSortOrder="ASC"
    //     onSort={() => console.log("onSort")}
    //     onSetPage={() => console.log("onSetPage")}
    //   />
    // </Wrapper>

    // <div>
    //   {/*<ul> {this.state.content} </ul>*/}
    //   <MenuGroup>
    //     <Section>
    //       <HeadingItem>CODEBASE</HeadingItem>
    //       <ButtonItem>Executives</ButtonItem>
    //     </Section>
    //     <Section hasSeparator>
    //       <ButtonItem>Project Managers</ButtonItem>
    //     </Section>
    //     <Section hasSeparator>
    //       <ButtonItem>Client Developers</ButtonItem>
    //     </Section>
    //   </MenuGroup>
    // </div>
  }
}

export default HelloWorld;
