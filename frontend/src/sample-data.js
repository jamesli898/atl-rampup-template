// @flow
/* sample-data.js */
import Avatar from "@atlaskit/avatar";
import React from "react";
import DropdownMenu, {
  DropdownItemGroup,
  DropdownItem
} from "@atlaskit/dropdown-menu";
import styled from "styled-components";
import codebase from "./codebase.json";
import lorem from "./lorem.json";

function createKey(input) {
  return input ? input.replace(/^(the|a|an)/, "").replace(/\s/g, "") : input;
}

function getRandomString() {
  return `${lorem[Math.floor(Math.random() * lorem.length)]}`;
}

const NameWrapper = styled.span`
  display: flex;
  align-items: center;
`;

const AvatarWrapper = styled.div`
  margin-right: 8px;
`;

export const caption = "List of CodeBase members";

export const createHead = () => {
  return {
    cells: [
      {
        key: "name",
        content: "Name",
        isSortable: true,
        width: 25
      },
      {
        key: "party",
        content: "Position",
        shouldTruncate: true,
        isSortable: true,
        width: 15
      },
      {
        key: "term",
        content: "Graduation Year",
        shouldTruncate: true,
        isSortable: true,
        width: 10
      },
      {
        key: "Contact",
        content: "Contact",
        shouldTruncate: true
      },
      {
        key: "more",
        shouldTruncate: true
      }
    ]
  };
};

export const head = createHead(true);

export const rows = codebase.map((codebase, index) => ({
  key: `row-${index}-${codebase.nm}`,
  cells: [
    {
      key: createKey(codebase.Name),
      content: (
        <NameWrapper>
          <AvatarWrapper>
            <Avatar
              name={codebase.Name}
              size="medium"
              src={
                "https://pixabay.com/photos/rose-blue-flower-bloom-romance-165819/"
              }
            />
          </AvatarWrapper>
          <a href={codebase.Github}>{codebase.Name}</a>
        </NameWrapper>
      )
    },
    {
      key: createKey(codebase.Role),
      content: codebase.Role
    },
    {
      key: codebase.Gradyear,
      content: codebase.Gradyear
    },
    {
      key: codebase.Email,
      content: codebase.Email
    },
    {
      content: (
        <DropdownMenu trigger="More" triggerType="button">
          <DropdownItemGroup>
            <DropdownItem>{codebase.nm}</DropdownItem>
          </DropdownItemGroup>
        </DropdownMenu>
      )
    }
  ]
}));
