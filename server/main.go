package main

import (
	"log"
	"net/http"

	"./router"
)

func main() {

	r := router.Router()
	log.Println("Now server is running on port 8080")
	log.Fatal(http.ListenAndServe(":8080", r))

}
