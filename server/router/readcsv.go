package router

import (
	"bufio"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
)

type Person struct {
	Name      string `json:"Name"`
	Role      string `json:"Role"`
	Gradyear  string `json:"Gradyear"`
	Email     string `json:"Email"`
	Phone     string `json:"Phone"`
	Birthday  string `json:"Birthday"`
	Snapchat  string `json:"Snapchat"`
	Instagram string `json:"Instagram"`
	Linkedin  string `json:"Linkedin"`
	Github    string `json:"Github"`
	Other     string `json:"Other"`
}

func readcsv() []Person {
	csvFile, _ := os.Open("ActiveRoster.csv")
	reader := csv.NewReader(bufio.NewReader(csvFile))
	var people []Person
	for {
		line, error := reader.Read()
		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
		}
		people = append(people, Person{
			Name:      line[0],
			Role:      line[1],
			Gradyear:  line[2],
			Email:     line[3],
			Phone:     line[4],
			Birthday:  line[5],
			Snapchat:  line[6],
			Instagram: line[7],
			Linkedin:  line[8],
			Github:    line[9],
			Other:     line[10],
		})
	}
	peopleJSON, _ := json.Marshal(people)
	fmt.Println(string(peopleJSON))

	return people
}
